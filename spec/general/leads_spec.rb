require 'spec_helper'


shared_examples "Create lead, add note and check it" do |last_name, company_name, note|
  it "Open add lead form" do
    on(LeadsPage).add_new_lead_element.when_visible.click
  end

  it "Enter lead last name" do
    on(LeadsPage).lead_lastname_element.when_visible(10).value = last_name
  end

  it "Enter lead company name" do
    on(LeadsPage).lead_companyname_element.when_visible(10).value = company_name
  end

  it "Save lead" do
    on(LeadsPage).create_lead
  end

  it "Enter note content" do
    on(LeadsPage).note_content_element.when_visible(10).value = note
  end

  it "Save note" do
    on(LeadsPage).create_note
  end

  it "Check if note is visible on list" do
    expect(@current_page.div_element(:css => ".item-body").text).to eq note
  end
end

describe "Leads" do
  before(:all) do
    login_to_autotest
    visit(LeadsPage)
  end

  describe "Add floating lead" do
    include_examples "Create lead, add note and check it", Faker::Name.last_name, Faker::Company.name, Faker::Lorem.sentence
  end
end