module LeadModal
  include PageObject

  text_field(:lead_lastname, :css => "#lead-last-name")
  text_field(:lead_companyname, :name => "company_name")
  button(:create_lead, :text => "Save")
end