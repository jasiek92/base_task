module LeadPage
  include PageObject

  textarea(:note_content, :name => "note")
  button(:create_note, :text => "Save")
end