class LeadsPage < AbstractPage
  include PageObject
  include LeadModal
  include LeadPage
  include RelatedToPicker

  page_url "https://app.futuresimple.com/leads"

  a(:add_new_lead, :css => "#leads-new")
end